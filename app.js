const express = require("express");
const { Sequelize, DataTypes } = require("sequelize");
const app = express();
const csv = require("csvtojson");
const jsonexport = require("jsonexport");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const nizStavki = [];

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// dodao staticko serviranje fajlova
app.use(express.static("static/html"));
app.use(express.static("static/js"));
app.use(express.static("static/css"));

// Import modela
//const Aktivnost = require("./modeli/aktivnost.js");
//const Dan = require("./modeli/dan.js");
//const Grupa = require("./modeli/grupa.js");
//const Predmet = require("./modeli/predmet.js");
//const Student = require("./modeli/student.js");
//const Tip = require("./modeli/tip.js");

// Database
const db = require("./db.js");

// Relacije //TODO

// modul za upisivanje u csv
const csvWriter = createCsvWriter({
  path: "./csv/raspored.csv",
  header: [
    { id: "naziv", title: "naziv" },
    { id: "aktivnost", title: "aktivnost" },
    { id: "dan", title: "dan" },
    { id: "pocetak", title: "pocetak" },
    { id: "kraj", title: "kraj" },
  ],
});

const stavkaRasporeda = {
  naziv: "",
  aktivnost: "",
  dan: "",
  pocetak: "",
  kraj: "",
};

function kojiJeDan(dan) {
  if (
    dan === "ponedeljak" ||
    dan === "ponedjeljak" ||
    dan === "Ponedeljak" ||
    dan === "Ponedjeljak"
  )
    return 0;
  if (dan === "utorak" || dan === "Utorak") return 1;
  if (dan === "srijeda" || dan === "Srijeda") return 2;
  if (
    dan === "cetvrtak" ||
    dan === "četvrtak" ||
    dan === "Četvrtak" ||
    dan === "Cetvrtak"
  )
    return 3;
  if (dan === "petak" || dan === "Petak") return 4;
  if (dan === "subota" || dan === "Subota") return 5;
  if (
    dan === "nedelja" ||
    dan === "nedjelja" ||
    dan === "Nedjelja" ||
    dan === "Nedelja"
  )
    return 6;
}

// CRUD operacije za sve modele

// READ

app.get("/v2/dan", (req, res) => {
  db.dan.findAll().then((dani) => res.send(dani));
});

app.get("/v2/aktivnost", (req, res) => {
  db.aktivnost.findAll().then((aktivnosti) => res.send(aktivnosti));
});

app.get("/v2/grupa", (req, res) => {
  db.grupa.findAll().then((grupe) => res.send(grupe));
});

app.get("/v2/predmet", (req, res) => {
  db.predmet.findAll().then((predmet) => res.send(predmet));
});

app.get("/v2/student", (req, res) => {
  db.student.findAll().then((student) => res.send(student));
});

app.get("/v2/tip", (req, res) => {
  db.tip.findAll().then((tip) => res.send(tip));
});

///////////////////// CREATE
app.post("/v2/aktivnost", async (req, res) => {
  const naziv = req.query.naziv;
  const pocetak = req.query.pocetak;
  const kraj = req.query.kraj;

  if (
    typeof naziv === "undefined" ||
    typeof pocetak === "undefined" ||
    typeof kraj === "undefined"
  ) {
    res
      .sendStatus(400)
      .send("Unesite neophodne parametre (naziv, pocetak, kraj)");
  }

  await db.aktivnost.create({ naziv: naziv, pocetak: pocetak, kraj: kraj });
  res.sendStatus(200);
});

app.post("/v2/dan", async (req, res) => {
  const naziv = req.query.naziv;
  if (typeof naziv === "undefined") {
    res.sendStatus(400).send("Unesite naziv dana");
  }
  // Create a new dan
  await Dan.create({ naziv: naziv });
  res.sendStatus(200);
});

app.post("/v2/grupa/", async (req, res) => {
  var data = req.body;
  const naziv = req.query.naziv;

  if (naziv)
    data = {
      naziv: naziv,
    };

  if (typeof naziv === "undefined" && typeof data === "undefined")
    res.status(400).send("Unesite naziv grupe");

  // Create a new grupa
  await db.grupa.create({ naziv: data.naziv }).catch((err) => console.log(err));
  res.status(200).send("Grupa uspjesno dodana!");
});

app.post("/v2/studentGrupa", async (req, res) => {
  const data = req.body;
  await db.studentGrupa
    .create({
      student: data.student,
      grupa: data.grupa,
    })
    .catch((err) => {
      console.log(err);
    });

  res.status(200).send("dodan novi red u tabeli studentGrupa");
});

app.post("/v2/predmet", async (req, res) => {
  const naziv = req.query.naziv;
  if (typeof naziv === "undefined") {
    res.sendStatus(400).send("Unesite naziv predmeta");
  }
  // Create a new predmet
  await db.predmet.create({ naziv: naziv });
  res.sendStatus(200);
});

// Morao sam koristii indeks (umjesto index) jer je  u mysql index rezervisana rijec
app.post("/v2/student", async (req, res) => {
  var data = req.body;
  const ime = req.query.ime;
  const indeks = req.query.indeks;

  if (ime && indeks) {
    data = {
      ime: ime,
      indeks: indeks,
    };
  } else if (!data) {
    res.status(400).send("Unesite ime i indeks");
  }
  db.student
    .findAll({
      where: {
        indeks: data.indeks,
        // ime: data.ime,
      },
      raw: true,
    })
    .then(async (students) => {
      if (students.length > 0) {
        res
          .status(400)
          .send(
            "Student " +
              data.ime +
              " nije kreiran jer postoji student " +
              students[0].ime +
              " sa istim indeksom " +
              students[0].indeks
          );
      } else {
        await db.student.create({
          ime: data.ime,
          indeks: data.indeks,
        });

        res
          .status(200)
          .send(
            "Student " + data.ime + " sa indeksom " + data.indeks + " kreiran!"
          );
      }
    })
    .catch((err) => console.log(err));
});

app.post("/v2/tip", async (req, res) => {
  const naziv = req.query.naziv;
  if (typeof naziv === "undefined") {
    res.sendStatus(400).send("Unesite naziv tipa");
  }
  // Create a new predmet
  await db.tip.create({ naziv: naziv });
  res.sendStatus(200);
});

///////////////////DELETE
app.delete("/v2/aktivnost/:id", async (req, res) => {
  const id = req.params.id;
  await Aktivnost.destroy({
    where: {
      id: id,
    },
  }).then((t) => {
    console.log("Broj obrisanih slogova " + t);
    res.sendStatus(200);
  });
});

app.delete("/v2/dan/:id", async (req, res) => {
  const id = req.params.id;
  await db.dan
    .destroy({
      where: {
        id: id,
      },
    })
    .then((t) => {
      console.log("Broj obrisanih slogova " + t);
      res.sendStatus(200);
    });
});

app.delete("/v2/grupa/:id", async (req, res) => {
  const id = req.params.id;
  await db.grupa
    .destroy({
      where: {
        id: id,
      },
    })
    .then((t) => {
      console.log("Broj obrisanih slogova " + t);
      res.sendStatus(200);
    });
  // res.sendStatus(400).send("Nesto ne valja");
});

app.delete("/v2/predmet/:id", async (req, res) => {
  const id = req.params.id;
  await db.predmet
    .destroy({
      where: {
        id: id,
      },
    })
    .then((t) => {
      console.log("Broj obrisanih slogova " + t);
      res.sendStatus(200);
    });
  //res.sendStatus(400).send("Nesto ne valja");
});

app.delete("/v2/student/:id", async (req, res) => {
  const id = req.params.id;
  await db.student
    .destroy({
      where: {
        id: id,
      },
    })
    .then((t) => {
      console.log("Broj obrisanih slogova " + t);
      res.sendStatus(200);
    });
  //res.sendStatus(400).send("Nesto ne valja");
});

app.delete("/v2/tip/:id", async (req, res) => {
  const id = req.params.id;
  await db.tip
    .destroy({
      where: {
        id: id,
      },
    })
    .then((t) => {
      console.log("Broj obrisanih slogova " + t);
      res.sendStatus(200);
    });
  //res.sendStatus(400).send("Nesto ne valja");
});

////////////////UPDATE

app.put("/v2/aktivnost/:id", async (req, res) => {
  const data = req.body;
  //data = JSON.parse(data);
  const id = req.params.id;

  if (data.naziv && data.pocetak && data.kraj) {
    await db.aktivnost
      .update(
        { naziv: data.naziv, pocetak: data.pocetak, kraj: data.kraj },
        { where: { id: id } }
      )
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.naziv && data.pocetak) {
    await db.aktivnost
      .update(
        { naziv: data.naziv, pocetak: data.pocetak },
        { where: { id: id } }
      )
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.naziv && data.kraj) {
    await db.aktivnost
      .update({ naziv: data.naziv, kraj: data.kraj }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.pocetak && data.kraj) {
    await db.aktivnost
      .update({ pocetak: data.pocetak, kraj: data.kraj }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.naziv) {
    await db.aktivnost
      .update({ naziv: data.naziv }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.pocetak) {
    await db.aktivnost
      .update({ pocetak: data.pocetak }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.kraj) {
    await db.aktivnost
      .update({ kraj: data.kraj }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else {
    res.sendStatus(400);
  }
});

app.put("/v2/dan/:id", async (req, res) => {
  const data = req.body;
  const id = req.params.id;

  await db.dan
    .update({ naziv: data.naziv }, { where: { id: id } })
    .then((t) => {
      console.log("Broj izmijenjenih slogova " + t);
      res.sendStatus(200);
    });
});

app.put("/v2/grupa/:id", async (req, res) => {
  const data = req.body;
  const id = req.params.id;

  await db.grupa
    .update({ naziv: data.naziv }, { where: { id: id } })
    .then((t) => {
      console.log("Broj izmijenjenih slogova " + t);
      res.sendStatus(200);
    });
});

app.put("/v2/predmet/:id", async (req, res) => {
  const data = req.body;
  const id = req.params.id;

  await db.predmet
    .update({ naziv: data.naziv }, { where: { id: id } })
    .then((t) => {
      console.log("Broj izmijenjenih slogova " + t);
      res.sendStatus(200);
    });
});

app.put("/v2/student/:id", async (req, res) => {
  const data = req.body;
  const id = req.params.id;

  if (data.ime && data.indeks) {
    await db.student
      .update({ ime: data.ime, indeks: data.indeks }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.ime) {
    await db.student
      .update({ ime: data.ime }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  } else if (data.indeks) {
    await db.student
      .update({ indeks: data.indeks }, { where: { id: id } })
      .then((t) => {
        console.log("Broj izmijenjenih slogova " + t);
        res.sendStatus(200);
      });
  }
});

app.put("/v2/tip/:id", async (req, res) => {
  const data = req.body;
  const id = req.params.id;

  await db.tip
    .update({ naziv: data.naziv }, { where: { id: id } })
    .then((t) => {
      console.log("Broj izmijenjenih slogova " + t);
      res.sendStatus(200);
    });
});

// prikaz glavne stranice (promijenio rutu za spiralu 4)
app.get("/", function (req, res) {
  res.sendFile(__dirname + "/static/html/studenti.html");
});

app.post("/raspored", async function (req, res) {
  // Uzimanje podataka sa forme
  const data = req.body;

  stavkaRasporeda.naziv = data.naziv;
  stavkaRasporeda.aktivnost = data.aktivnost;
  stavkaRasporeda.dan = data.dan;
  stavkaRasporeda.pocetak = data.pocetak;
  stavkaRasporeda.kraj = data.kraj;
  // provjera specijalnih karaktera (osim "-" npr mur1-grupa1)
  let regex = /^[A-Za-z0-9-]+$/;
  let isValid = true;
  let isValidNaziv = regex.test(stavkaRasporeda.naziv);
  let isValidAktivnost = regex.test(stavkaRasporeda.aktivnost);
  let isValidDan = regex.test(stavkaRasporeda.dan);
  if (!isValidNaziv || !isValidAktivnost || !isValidDan) {
    isValid = false;
    `naziv=${naziv}&aktivnost=${aktivnost}&dan=${dan}&pocetak=${pocetak}&kraj=${kraj}`;

    const ajax = new XMLHttpRequest();

    res.send(
      "Polja naziv, aktivnost i dan ne smiju sadrzavati specijalne karaktere"
    );
  }
  if (isValid) {
    // provjera poklapanja
    let poklapanje = false;
    const convert = await csv()
      .fromFile("./csv/raspored.csv")
      .then((json) => {
        json.forEach((row) => {
          //  console.log(row);
          if (
            row.dan === stavkaRasporeda.dan &&
            ((row.pocetak < stavkaRasporeda.pocetak &&
              stavkaRasporeda.kraj < row.kraj) ||
              (stavkaRasporeda.pocetak < row.kraj &&
                stavkaRasporeda.kraj > row.kraj) ||
              (stavkaRasporeda.pocetak < row.pocetak &&
                stavkaRasporeda.kraj > row.pocetak) ||
              (row.pocetak === stavkaRasporeda.pocetak &&
                row.kraj === stavkaRasporeda.kraj) ||
              (row.pocetak === stavkaRasporeda.pocetak &&
                stavkaRasporeda.kraj < row.kraj) ||
              (stavkaRasporeda.pocetak > row.pocetak &&
                row.kraj === stavkaRasporeda.kraj))
          ) {
            poklapanje = true;
            res.send("Doslo je do poklapanja! ispravite termin");
            res.status(400).end();
          }
        });
      })
      .then(() => {
        //   console.log("Procitao");
        if (!poklapanje) {
          // nizStavki.push(stavkaRasporeda);
          // csvWriter.writeRecords(nizStavki).then(() => {
          //  console.log("podaci uspjesno upisani u csv fajl! ");
          //});
          db.stavkaRasporeda
            .create({
              naziv: stavkaRasporeda.naziv,
              aktivnost: stavkaRasporeda.aktivnost,
              dan: stavkaRasporeda.dan,
              pocetak: stavkaRasporeda.pocetak,
              kraj: stavkaRasporeda.kraj,
            })
            .then((t) => {
              res.status(200).send("stavka rasporeda uspjesno dodana!");
            });
          nizStavki.pop();
        }
      });
  }
});

// Za testiranje slanja Accept u headeru koristio sam postman
// Da bi lakše provjerili da li sortiranje radi preporučujem da se smanji prozor pretraživača (tako će se sve prikazati u jednoj koloni))
app.get("/raspored", async (req, res) => {
  let nizStavki = [];
  csv()
    .fromFile("./csv/raspored.csv")
    .then((json) => {
      json.forEach((row) => {
        nizStavki.push(row);
      });
    })
    .then(() => {
      //ako pretrazuje po danu iz niza uzimam samo objekte u kojima je dan jednak traženom danu
      if (req.query.dan) {
        nizStavki = nizStavki.filter((stavka) => stavka.dan === req.query.dan);
      }
      //ako trazi sort
      if (req.query.sort) {
        const dir = req.query.sort.slice(0, 1);
        const cond = req.query.sort.slice(1);
        if (cond === "dan") {
          nizStavki = nizStavki.sort((a, b) => daySort(a, b, cond));
        } else nizStavki = nizStavki.sort((a, b) => dataSort(a, b, cond)); // sortita Ascending
        if (dir === "D") nizStavki.reverse(); // ukoliko se trazilo Descending, samo okrenem niz
      }
      // ako u headeru ima Accept
      if (req.get("Accept") === "text/csv") {
        jsonexport(nizStavki, function (err, csv) {
          if (err) return console.log(err);
          res.send(csv);
          nizStavki.splice(0, nizStavki.length); // Ovo koristim da restartujem (ispraznim) niz nakon slanja u kojem se nalaze podaci u zavisnosti od onoga sto je trazeno
          res.end();
        });
      } else {
        res.send(nizStavki).end();
        nizStavki.splice(0, nizStavki.length); // Ovo koristim da restartujem (ispraznim) niz nakon slanja u kojem se nalaze podaci u zavisnosti od onoga sto je trazeno
      }
    });
});
// NE VALJA PRETRAGA JOS UVIJEK
app.get("/predmet", async (req, res) => {
  let trazeni = req.query.predmet;

  let nizStavki = [];
  csv()
    .fromFile("./csv/raspored.csv")
    .then((json) => {
      json.forEach((row) => {
        nizStavki.push(row);
      });
    })
    .then(() => {
      if (req.query.predmet) {
        nizStavki = nizStavki.filter((stavka) => {
          stavka.predmet === trazeni;
        });
      }
      if (nizStavki.length === 0) {
        console.log("Nema");
        res.sendStatus(200);
      } else {
        res.sendStatus(404);
        console.log("Ima");
      }
    });
});

app.listen(8080);
console.log("Server started on port 8080");

function dataSort(a, b, cond) {
  if (a[cond] > b[cond]) {
    console.log(a[cond] + " vece " + b[cond]);
    return 1;
  } else if (a[cond] < b[cond]) {
    console.log(a[cond] + " manje " + b[cond]);
    return -1;
  }
  return 0;
}
// za sortiranje prema danima u sedmici (ne po slovima sto je i logično)
function daySort(a, b, cond) {
  if (kojiJeDan(a[cond]) > kojiJeDan(b[cond])) {
    console.log(a[cond] + " vece " + b[cond]);
    return 1;
  } else if (kojiJeDan(a[cond]) < kojiJeDan(b[cond])) {
    console.log(a[cond] + " manje " + b[cond]);
    return -1;
  }
  return 0;
}
