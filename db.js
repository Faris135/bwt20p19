const { Sequelize, DataTypes } = require("sequelize");
var opts = {
  define: {
    timestamps: false,
    logging: false,
    freezeTableName: true,
  },
};
const db = {};

const sequelize = new Sequelize("mysql://root:@localhost:3306/wt2019st", opts);

//const sequelize = new Sequelize("wt2019st", "root", "root", {
//  host: "127.0.0.1",
//  dialect: "mysql",
//});

// Testing connection
try {
  sequelize.authenticate();
  console.log("Connection has been established successfully.");
} catch (error) {
  console.error("Unable to connect to the database:", error);
}

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.aktivnost = require(__dirname + "/modeli/aktivnost.js")(
  sequelize,
  DataTypes
);
db.dan = require(__dirname + "/modeli/dan.js")(sequelize, DataTypes);
db.grupa = require(__dirname + "/modeli/grupa.js")(sequelize, DataTypes);
db.predmet = require(__dirname + "/modeli/predmet.js")(sequelize, DataTypes);
db.student = require(__dirname + "/modeli/student.js")(sequelize, DataTypes);
db.tip = require(__dirname + "/modeli/tip.js")(sequelize, DataTypes);
db.studentGrupa = require(__dirname + "/modeli/studentGrupa.js")(
  sequelize,
  DataTypes
);
db.stavkaRasporeda = require(__dirname + "/modeli/stavkaRasporeda.js")(
  sequelize,
  DataTypes
);

//Predmet 1-N Grupa
db.predmet.hasMany(db.grupa, { as: "predmetid", foreignKey: "predmetID" });
//Aktivnost N-1 Predmet
db.aktivnost.hasMany(db.predmet, {
  as: "predmet_aktivnosti_id",
  foreignKey: "aktivnostID",
});
//Aktivnost N-0 Grupa
db.aktivnost.hasMany(db.grupa, {
  as: "grupa_aktivnosti_id",
  foreignKey: "aktivnostID",
  allowNull: true,
});
//Aktivnost N-1 Dan
db.aktivnost.hasMany(db.dan, {
  as: "dan_aktivnosti_id",
  foreignKey: "aktivnostID",
});
//Aktivnost N-1 Tip
db.aktivnost.hasMany(db.tip, {
  as: "tip_aktivnosti_id",
  foreignKey: "aktivnostID",
});
//Student N-M Grupa
db.student.belongsToMany(db.grupa, {
  as: "grupe",
  through: "studentGrupa",
  foreignKey: "student",
});
db.grupa.belongsToMany(db.student, {
  as: "studenti",
  through: "studentGrupa",
  foreignKey: "grupa",
});

module.exports = db;
