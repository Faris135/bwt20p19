module.exports = (sequelize, DataTypes) => {
  const Aktivnost = sequelize.define("aktivnost", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    naziv: DataTypes.STRING,
    pocetak: DataTypes.FLOAT,
    kraj: DataTypes.FLOAT,
  });
  return Aktivnost;
};
