module.exports = (sequelize, DataTypes) => {
  const Dan = sequelize.define("dan", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    naziv: DataTypes.STRING,
  });
  return Dan;
};
