module.exports = function (sequelize, DataTypes) {
  const Grupa = sequelize.define("grupa", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    naziv: DataTypes.STRING,
    predmetID: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    aktivnostID: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
  });
  return Grupa;
};
