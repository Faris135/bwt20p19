module.exports = (sequelize, DataTypes) => {
  const Predmet = sequelize.define("predmet", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    naziv: DataTypes.STRING,
  });
  return Predmet;
};
