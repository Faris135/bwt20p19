module.exports = (sequelize, DataTypes) => {
  const stavkaRasporeda = sequelize.define("stavkaRasporeda", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoincrement: true,
    },
    naziv: DataTypes.STRING,
    aktivnost: DataTypes.STRING,
    dan: DataTypes.STRING,
    pocetak: DataTypes.FLOAT,
    kraj: DataTypes.FLOAT,
  });
  return stavkaRasporeda;
};
