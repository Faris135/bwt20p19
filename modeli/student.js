module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define("student", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    indeks: DataTypes.STRING,
    ime: DataTypes.STRING,
    studentGrupaID: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
  });
  return Student;
};
