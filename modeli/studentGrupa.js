module.exports = (sequelize, DataTypes) => {
  const StudentGrupa = sequelize.define("studentGrupa", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoincrement: true,
    },
    student: DataTypes.INTEGER,
    grupa: DataTypes.STRING,
  });
  return StudentGrupa;
};
