module.exports = (sequelize, DataTypes) => {
  const Tip = sequelize.define("tip", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    naziv: DataTypes.STRING,
  });
  return Tip;
};
