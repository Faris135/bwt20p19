function ucitajSortirano(dan, atribut, callback) {
  //posto nije radilo kada je dan null, dan postavljam na prazan string da bi vratio sve iz csv fajla
  if (dan === null) dan = "";
  const ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      callback(JSON.parse(this.responseText), null);
    } else if (ajax.readyState == 4 && this.status != 200) {
      callback(null, this.responseText);
    }
  };
  //koristim uradjeni get zahtjev za vraćanje podataka iz csv, string template za slanje atributa
  ajax.open("GET", `/raspored?dan=${dan}&sort=${atribut}`);
  ajax.send();
}
