class GoogleMeet {
  static dajZadnjePredavanje(htmlString) {
    const doc = new DOMParser().parseFromString(htmlString, "text/html");
    const aTag = doc.getElementsByTagName("a");
    const nizTagovaZaPredavanje = [];
    for (let link of aTag) {
      if (
        link.innerHTML.includes("predavanj") &&
        link.href.indexOf("meet.google.com") > -1
      ) {
        nizTagovaZaPredavanje.push(link);
      }
    }
    if (nizTagovaZaPredavanje.lenght == 0) return null;
    // console.log(
    //  "Zadnje predavanje" +
    //   nizTagovaZaPredavanje[nizTagovaZaPredavanje.length - 1]
    //);
    return nizTagovaZaPredavanje[nizTagovaZaPredavanje.length - 1];
  }

  static dajZadnjuVjezbu(htmlString) {
    const doc = new DOMParser().parseFromString(htmlString, "text/html");
    const aTag = doc.getElementsByTagName("a");
    const nizTagovaZaVjezbe = [];
    for (let link of aTag) {
      if (
        (link.innerHTML.includes("vjezb") ||
          link.innerHTML.includes("vježb")) &&
        link.href.indexOf("meet.google.com") > -1
      ) {
        console.log("vjezbe" + link.innerHTML);

        nizTagovaZaVjezbe.push(link);
      }
    }
    if (nizTagovaZaVjezbe.lenght == 0) return null;
    return nizTagovaZaVjezbe[nizTagovaZaVjezbe.length - 1];
  }
}
