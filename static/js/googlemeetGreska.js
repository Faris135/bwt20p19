class GoogleMeet {
  static dajZadnjePredavanje(htmlString) {
    const doc = new DOMParser().parseFromString(htmlString, "text/html");
    const aTag = doc.getElementsByTagName("a");
    const nizTagovaZaPredavanje = [];
    for (let link of aTag) {
      nizTagovaZaPredavanje.push(link);
    }

    return nizTagovaZaPredavanje[nizTagovaZaPredavanje.length - 1];
  }

  static dajZadnjuVjezbu(htmlString) {
    const doc = new DOMParser().parseFromString(htmlString, "text/html");
    const aTag = doc.getElementsByTagName("a");
    const nizTagovaZaVjezbe = [];
    for (let link of aTag) {
      console.log("op" + link);
      nizTagovaZaVjezbe.push(link);
      console.log(link);
    }
    return nizTagovaZaVjezbe[nizTagovaZaVjezbe.length - 1];
  }
}
