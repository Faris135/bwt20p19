let order = "A";

// primam podatke u json formatu
function ucitajTabelu(data, error) {
  if (error) return;
  // niz u koji cu primati elmenete koje vracam u body tabele
  let rows = [];
  // od svakog elementa u nizu pravim red za tabelu
  data.forEach((element) =>
    rows.push(
      `<tr><td>${element.naziv}</td><td>${element.aktivnost}</td><td>${element.dan}</td><td>${element.pocetak}</td><td>${element.kraj}</td></tr>`
    )
  );
  //u body tabele postavljam niz redova
  document.getElementById("container").innerHTML = rows.join("");
}
// Primam event koji predstavlja header koji je kliknut
function sortPoAtributu(event) {
  let atribut = event.target.innerHTML.toLowerCase();
  atribut = order + atribut;
  // Globalna varijabla koja odredjuje da li sortiram asd ili desc
  order = order === "A" ? "D" : "A";
  // pozivamo
  ucitajSortirano("", atribut, ucitajTabelu);
}
