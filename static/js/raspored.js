raspored = `
BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30
`;

function parseCSV(stringCsv) {
  strDelimiter = ",";
  var objPattern = new RegExp(
    "(\\" +
      strDelimiter +
      "|\\r?\\n|\\r|^)" +
      '(?:"([^"]*(?:""[^"]*)*)"|' +
      '([^"\\' +
      strDelimiter +
      "\\r\\n]*))",
    "gi"
  );

  var arrData = [[]];
  var arrMatches = null;
  while ((arrMatches = objPattern.exec(stringCsv))) {
    var strMatchedDelimiter = arrMatches[1];
    if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
      arrData.push([]);
    }
    var strMatchedValue;
    if (arrMatches[2]) {
      strMatchedValue = arrMatches[2].replace(new RegExp('""', "g"), '"');
    } else {
      strMatchedValue = arrMatches[3];
    }
    arrData[arrData.length - 1].push(strMatchedValue);
  }
  return arrData;
}

class Raspored {
  nizDogadjaj = [];
  constructor(raspored) {
    this.nizDogadjaj = parseCSV(raspored);
  }

  ispisi() {
    for (let i = 1; i < this.nizDogadjaj.length - 1; i++) {
      for (let j = 0; j < 5; j++) {
        console.log(this.nizDogadjaj[i][j]);
      }
    }
  }

  kojiJeDan(indeks) {
    const dani = [
      "ponedjeljak",
      "utorak",
      "srijeda",
      "cetvrtak",
      "petak",
      "subota",
      "nedelja",
    ];
    return dani[indeks - 1];
  }

  appendLeadingZeroes(n) {
    if (n <= 9) {
      return "0" + n;
    }
    return n;
  }

  formatiraj(date) {
    return (
      date.getDate() +
      "-" +
      this.appendLeadingZeroes(date.getMonth() + 1) +
      "-" +
      this.appendLeadingZeroes(date.getFullYear()) +
      "T" +
      this.appendLeadingZeroes(date.getHours()) +
      ":" +
      this.appendLeadingZeroes(date.getMinutes()) +
      ":" +
      this.appendLeadingZeroes(date.getSeconds())
    );
  }
  smanjiSat(vrijeme) {
    //   console.log(vrijeme);
    let prviDio = vrijeme.split("T");
    let datum = prviDio[0].split("-");
    let datumUMomFormatu = datum[2] + "-" + datum[1] + "-" + datum[0];
    // console.log(datumUMomFormatu);
    let date = new Date(datumUMomFormatu + "T" + prviDio[1]);
    // console.log(date);
    date.setMinutes(date.getMinutes() - 1);
    let noviD = this.formatiraj(date);
    //  console.log(noviD);
    return noviD;
  }

  povecajSat(vrijeme) {
    //   console.log(vrijeme);
    let prviDio = vrijeme.split("T");
    let datum = prviDio[0].split("-");
    let datumUMomFormatu = datum[2] + "-" + datum[1] + "-" + datum[0];
    // console.log(datumUMomFormatu);
    let date = new Date(datumUMomFormatu + "T" + prviDio[1]);
    // console.log(date);
    date.setMinutes(date.getMinutes() + 1);
    let noviD = this.formatiraj(date);
    //  console.log(noviD);
    return noviD;
  }

  dajTrenutnu(vrijeme, naziv) {
    // Parsiram rucno datum, dio sa danom, i dio sam satom,minutom
    let prviDio = vrijeme.slice(0, 10);
    let datum = prviDio.split("-");
    let datumUMomFormatu = datum[1] + "-" + datum[0] + "-" + datum[2];
    let date = new Date(datumUMomFormatu);
    let brojDana = date.getDay();
    let dan = this.kojiJeDan(brojDana);
    var sat = vrijeme.slice(11, 13);
    var minuta = vrijeme.slice(14, 16);
    var sekunde = parseInt(sat) * 60 * 60 + parseInt(minuta) * 60;

    for (let i = 1; i < this.nizDogadjaj.length - 1; i++) {
      // PRVO TRAZIM DAN

      if (this.nizDogadjaj[i][2] == dan) {
        // console.log("NASAO DAN");
        this.indeksPrethodnogUIstomDanu = i;
        //  console.log("Isti dan " + this.nizDogadjaj[i][2]);

        // TRAZIM AKTIVNOST
        if (this.nizDogadjaj[i][0] === naziv || naziv === "") {
          // console.log("NASAO AKTIVNOST");
          // GLEDAM KAD JE POCELO
          let VrijemePocetkaPoRasporedu = this.nizDogadjaj[i][3];
          let pocetakPoRasporeduSat = VrijemePocetkaPoRasporedu.slice(0, 2);
          let pocetakPoRasporeduMinuta = VrijemePocetkaPoRasporedu.slice(3, 5);
          let pocetakPoRasporeduSekunde =
            parseInt(pocetakPoRasporeduSat) * 60 * 60 +
            parseInt(pocetakPoRasporeduMinuta) * 60;

          // GLEDAM DO KAD TRAJE
          let doKadTrajePoRasporedu = this.nizDogadjaj[i][4];
          let doKadTrajeSat = doKadTrajePoRasporedu.slice(0, 2);
          let doKadTrajeMinuta = doKadTrajePoRasporedu.slice(3, 5);
          let doKadtrajeSekunde =
            parseInt(doKadTrajeSat) * 60 * 60 + parseInt(doKadTrajeMinuta) * 60;

          // AKO JE POCELO I JOS NIJE ZAVRSILO
          if (
            pocetakPoRasporeduSekunde <= sekunde &&
            doKadtrajeSekunde >= sekunde
          ) {
            // IZRACUNAJ KOLIKO JE OSTALO I VRATI
            let ostalo = doKadtrajeSekunde - sekunde;
            let ostaloUMinutama = parseInt(ostalo) / 60;
            //let res = this.nizDogadjaj[i][0].trim();
            //res = res.split("-");

            return this.nizDogadjaj[i][0] + " " + ostaloUMinutama;
          }
        }
      }
    }
    // console.log("ZASTO");
    return null;
  }

  dajPrethodnu(vrijeme, naziv) {
    let novoV = this.smanjiSat(vrijeme);
    // console.log(novoV);
    let res = this.dajTrenutnu(novoV, naziv);
    // console.log(res);
    while (res === null) {
      novoV = this.smanjiSat(vrijeme);
      res = this.dajTrenutnu(novoV, naziv);
      vrijeme = novoV;
    }
    let sp = res.split(" ");
    return sp[0];
  }
  // NAJGLUPLJI MOGUĆI NAČIN DA SE OVO URADI, ALI RADI :)
  dajSljedecu(vrijeme, naziv) {
    let novoV = this.povecajSat(vrijeme);
    //  console.log(novoV);
    let res = this.dajTrenutnu(novoV, "");
    //  console.log(res);
    while (res === null) {
      novoV = this.povecajSat(vrijeme);
      res = this.dajTrenutnu(novoV, naziv);
      vrijeme = novoV;
    }

    return res;
  }
}
