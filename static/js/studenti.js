function ucitajUSelect() {
  var options = [];
  const ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let data = this.responseText;
      data = JSON.parse(data);
      for (let i = 0; i < data.length; i++) {
        options.push(`<option>${data[i].naziv}</option>`);
      }
      document.getElementById("grupe").innerHTML = options;
    } else if (ajax.readyState == 4 && this.status != 200) {
      console.log(this.responseText);
    }
  };
  ajax.open("GET", `/v2/grupa`);
  ajax.send();
}

async function UpisiStudente() {
  var studenti = document.getElementById("spisakStudenata").value;
  studenti = studenti.split(/\r?\n/);

  var nizNovihStud = [];
  for (let i = 0; i < studenti.length; i++) {
    var noviStud = { ime: "", indeks: "" };
    var elements = studenti[i].split(",");
    noviStud.ime = elements[0];
    noviStud.indeks = elements[1];
    nizNovihStud.push(noviStud);
  }
  //const data = `ime=${nizNovihStud[0].ime}&indeks=${nizNovihStud[0].indeks}`;

  for (let i = 0; i < nizNovihStud.length; i++) {
    const ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("spisakStudenata").value +=
          "\n\n" + this.responseText;
        const ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById("spisakStudenata").value +=
              "\n\n" + this.responseText;
          } else if (ajax.readyState == 4 && this.status != 200) {
            document.getElementById("spisakStudenata").value +=
              "\n\n" + this.responseText;
          }
        };
        ajax.open("POST", "/v2/studentGrupa", true);
        ajax.setRequestHeader(
          "Content-type",
          "application/x-www-form-urlencoded"
        );

        let grupa = document.getElementById("grupe").value;
        console.log(grupa);
        // TODO //popraviti zapisivanje id-a grupe
        ajax.send(`student=${nizNovihStud[i].indeks}&grupa=${grupa}`);
        // DO OVDJE IDE AJAX
      } else if (ajax.readyState == 4 && this.status != 200) {
        document.getElementById("spisakStudenata").value +=
          "\n\n" + this.responseText;
      }
    };
    ajax.open("POST", "/v2/student", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //console.log(`ime=${nizNovihStud[i].ime}&indeks=${nizNovihStud[i].indeks}`);
    ajax.send(`ime=${nizNovihStud[i].ime}&indeks=${nizNovihStud[i].indeks}`);
  }
}
async function dodajGrupu() {
  const grupa = document.getElementById("dodajGrupu").value;
  const ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("spisakStudenata").value +=
        "\n\n" + this.responseText;
    } else if (ajax.readyState == 4 && this.status != 200) {
      document.getElementById("spisakStudenata").value +=
        "\n\n" + this.responseText;
    }
  };
  ajax.open("POST", "/v2/grupa", true);
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send(`naziv=${grupa}`);
}
