//Test kada svaka druga sedmica ima vježbu (rezultat url vježbe iz posljednje sedmice)
//Test kada se link nalazi van ul liste (validan rezultat iz posljednje sedmice isključujuću link koji je val liste)
//Test kada su predavanja i vježbe samo u prvoj sedmici (rezultat url vježbe iz prve sedmice i url predavanja iz prve sedmice)
//Test kada nije validan html kod - kada ne sadrži div ‘course-content’ sa ul listom ‘weeks’ i elementima liste ‘section-*’.

let assert = chai.assert;
describe("Predavanja i vjezbe", function () {
  it("Test kada nema niti jednog predavanja", function () {
    // znamo da u primjerima string sa indeksom 3 nema predavanja
    assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[3]), null);
  });

  it("Test kada nema niti jedne vježbe", function () {
    // znamo da u primjerima string sa indeksom 4 nema vjezbi
    assert.equal(GoogleMeet.dajZadnjuVjezbu(primjeri[4]), null);
  });

  it("Test kada je string prazan", function () {
    // znamo da u primjerima string sa indeksom 0 nema predavanja
    assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[0].length, 0));
  });

  it("Test kada imaju linkovi sa ispravnim nazivima ali url ne sadrži meet.google.com", function () {
    // znamo da u primjerima string sa indeksom 4 ima link za c2, a ne za google meet
    assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[4].length, null));
  });

  it("Test kada postoje linkovi sa ispravnim url-om ali ne sadrže tekst ‘vjezb’,’vježb’ i ’predavanj’", function () {
    //// primjer[2] sam pokvario tako sto sam izbrisao rijeci "predavanje" da bih mogao testirati da se link nece vratiti jer nisu ispunjeni svi uslovi
    assert.equal(GoogleMeet.dajZadnjePredavanje(primjeri[3].length, null));
  });
});
