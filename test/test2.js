//dajPrethodnuAktivnost za slučaj kada je prva prethodna aktivnost vježba sa pogrešnom grupom, tada trebate vratiti aktivnost prije nje

let assert = chai.assert;
r = new Raspored(raspored);
describe("Raspored", function () {
  it("dajTrenutnuAktivnost kada nema trenutne aktivnosti u datom vremenu", function () {
    assert.equal(r.dajTrenutnu("17-04-2019T13:00:00", "FWT-grupa2"), null);
  });

  it("dajTrenutnuAktivnost na početku neke aktivnosti", function () {
    // saljemo početak predavanja iz mur1.
    assert.equal(r.dajTrenutnu("16-04-2019T09:00:00", "MUR1"), "MUR1 120");
  });

  it("dajTrenutnuAktivnost na kraju neke aktivnosti", function () {
    // saljemo kraj predavanja iz mur1 (MUR1, 0 minuta do kraja(zavrseno)).
    assert.equal(r.dajTrenutnu("16-04-2019T11:00:00", "MUR1"), "MUR1 0");
  });

  it("kada postoji vježba u datom vremenu i ispravna je grupa", function () {
    assert.equal(
      r.dajTrenutnu("17-04-2019T12:00:00", "MUR1-grupa1"),
      "MUR1-grupa1 30"
    );
  });

  it("dajTrenutnuAktivnost kada postoji vježba u datom vremenu ali nije ispravna grupa", function () {
    // Trenutno je grupa1, poslao grupu2
    assert.equal(r.dajTrenutnu("17-04-2019T12:00:00", "MUR1-grupa2"), null);
  });

  it(" dva ekvivalentna testa za dajSljedecuAktivnost kao za metodu dajPrethodnuAktivnost", function () {
    assert.equal(
      r.dajSljedecu("17-04-2019T12:00:00", "MUR1-grupa1"),
      "MUR1-grupa2 30"
    );
    assert.equal(
      r.dajPrethodnu("17-04-2019T13:00:00", "MUR1-grupa2"),
      "MUR1-grupa1"
    );
  });
  it("dajPrethodnuAktivnost prije prve aktivnosti u ponedjeljak, treba vratiti posljednju aktivnost iz petka, ili četvrtka ako nema aktivnosti u petak itd.", function () {
    assert.equal(
      r.dajPrethodnu("15-04-2019T06:00:00", "MUR1-grupa2"),
      "MUR1-grupa2"
    );
  });
});
